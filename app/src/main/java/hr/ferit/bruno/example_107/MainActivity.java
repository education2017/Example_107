package hr.ferit.bruno.example_107;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.ChangeImageTransform;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.Rotate;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.rlContainer) RelativeLayout rlContainer;
    @BindView(R.id.ivGhost) ImageView ivGhost;
    @BindView(R.id.tvGhostText) TextView tvGhostText;

    private boolean mIsFaded = true;
    private boolean mIsRotated = false;
    private boolean mIsLarge = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.bScare, R.id.bMove, R.id.bRotate})
    public void onClick(Button button){
        switch (button.getId()){
            case R.id.bScare: scare(); break;
            case R.id.bMove: move(); break;
            case R.id.bRotate: rotate(); break;
        }
    }

    private void rotate() {
        Transition transition = new Rotate();
        TransitionManager.beginDelayedTransition(rlContainer, transition);
        int rotation = this.mIsRotated ? 0 : 360;
        this.ivGhost.setRotation(rotation);
        this.mIsRotated = !this.mIsRotated;
    }

    private void move() {
        TransitionSet transitionSet = new TransitionSet()
                .addTransition(new ChangeBounds())
                .addTransition(new ChangeImageTransform());
        TransitionManager.beginDelayedTransition(rlContainer,transitionSet);

        RelativeLayout.LayoutParams params =
                (RelativeLayout.LayoutParams) ivGhost.getLayoutParams();
        if(this.mIsLarge){
            params.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
            params.width = RelativeLayout.LayoutParams.WRAP_CONTENT;
            ivGhost.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }else{
            params.height = RelativeLayout.LayoutParams.MATCH_PARENT;
            params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
            ivGhost.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
        ivGhost.setLayoutParams(params);
        this.mIsLarge = !this.mIsLarge;
    }

    private void scare() {
        Transition transition = new Fade();
        TransitionManager.beginDelayedTransition(rlContainer, transition);
        if(this.mIsFaded){
            this.ivGhost.setVisibility(View.VISIBLE);
            this.tvGhostText.setVisibility(View.VISIBLE);
        }else{
            this.ivGhost.setVisibility(View.GONE);
            this.tvGhostText.setVisibility(View.GONE);
        }
        this.mIsFaded = !this.mIsFaded;
    }
}
